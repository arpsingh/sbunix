#include <sys/defs.h>
#include <sys/interrupts.h>
#include <sys/sbunix.h>
#include <sys/process.h>
#include <sys/syscall.h>

#define NUM_SYSCALLS 2

extern task_struct *current;

int sys_write(uint64_t fd, uint64_t buffer, int size)
{
        size = puts((char *)buffer);

        return size;
}

static void* syscalls[NUM_SYSCALLS] =
{
//	sys_read,
	&sys_write,
//	sys_brk,
	&sys_fork,
/*	sys_execvpe,
	sys_wait,
	sys_waitpid,
	sys_exit,
	sys_yield,
	sys_mmap,
	sys_munmap,
	sys_getpid,
	sys_getppid,
	sys_listprocess,
	sys_opendir,
	sys_readdir,
	sys_closedir,
	sys_open,
	sys_close,
	sys_sleep,
	sys_clear,
	sys_lseek,
	sys_mkdir,
	sys_shutdown
*/
};

uint64_t syscalling_task_rsp; /* kernel stack */
uint64_t syscall_ret_address;
uint64_t syscalling_task_user_rsp; /* the user stack */
struct isr_regs *parent;
void syscall_handler(struct isr_regs *r)
{

	syscall_ret_address = r->rip;
	syscalling_task_rsp = r->rsp; 
	parent = r;
	uint64_t ret;

	void *sys_routine;
	//void *sys_routine = syscalls[r->rax];

	if(r->rax == 1)
		sys_routine = syscalls[0];
	else
		sys_routine = syscalls[1];


    __asm volatile (
     "pushq %%rdi;"
     "pushq %%rsi;"
     "pushq %%rbx;"
     "pushq %%rcx;"
     "pushq %%rdx;"
     "movq %1, %%rdi;"
     "movq %2, %%rsi;"
     "movq %3, %%rdx;"
     "movq %4, %%rcx;"
     "movq %5, %%r8;"
     "callq *%6;"
     "popq %%rdx;"
     "popq %%rcx;"
     "popq %%rbx;"
     "popq %%rsi;"
     "popq %%rdi;"
     : "=a" (ret)
     : "r" (r->rdi), "r" (r->rsi),  "r" (r->rbx),"r" (r->rcx), "r" (r->rdx), "r" (sys_routine)
     : "rsi","rdi","rbx","rcx","rdx"
     );

}

