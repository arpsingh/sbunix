#
# gdt.s
#
#  Created on: Dec 29, 2010
#      Author: cds
#

.text

######
# load a new GDT
#  parameter 1: address of gdtr
#  parameter 2: new code descriptor offset
#  parameter 3: new data descriptor offset
.global _x86_64_asm_lgdt
_x86_64_asm_lgdt:

	lgdt (%rdi)

	pushq %rsi                  # push code selector
	movabsq $.done, %r10
	pushq %r10                  # push return address
	lretq                       # far-return to new cs descriptor ( the retq below )
.done:
	movq %rdx, %es
	movq %rdx, %fs
	movq %rdx, %gs
	movq %rdx, %ds
	movq %rdx, %ss
	retq

.global tss_flush   # Allows our C code to call tss_flush().
tss_flush:		# 101000 0 11
	mov $0x2B, %ax     # Load the index of our TSS structure - The index is
                    	 # 0x28, as it is the 5th selector and each is 8 bytes
		    	 # long, but we set the bottom two bits (making 0x2B)
		    	 # so that it has an RPL of 3, not zero.
	ltr %ax           # Load 0x2B into the task state register.
        retq

