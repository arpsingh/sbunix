
/* © Copyright 2015, ggehlot@cs.stonybrook.edu
 * arpsingh@cs.stonybrook.edu,smehra@stonybrook.edu
 * Everyone is permitted to copy and distribute verbatim copies
 * of this license document, but changing it is not allowed.
 */

#include <sys/sbunix.h>
#include <sys/gdt.h>
#include <sys/tarfs.h>
#include <stdarg.h>
#include <sys/interrupts.h>
#include <sys/string.h>

int check_print_escape(const char *format); /*function to execute backspace*/
int print_to_console(const char *string, int size);/*function to print console*/
void update_linefeed();/*keeps track of cursor position*/
void clear_console();/*function to clear console*/
static void vprintf(const char *format,va_list args);/*function to excute actual functionality of vprintf*/

static char *v_ptr = (char *)0xFFFFFFFF800B8000;
static int line_no=0;
static int column_no=0;

static int stat = 0;

char *starting_address_of_VGA_buffer = (char *)0xFFFFFFFF800B8000;
int check_print_escape(const char *format){
        if (*format == '\n' )
        {
		print_newline();
                /*while (column_no != 0 ) {
                        print_to_console(sp,1);
                }*/
                return 1;
        } else
                return 0;

}

void update_linefeed() {

        if (column_no == 79) {
                column_no = 0;

                if (line_no == 24) {
                        line_no =0;
        //              clear_console();/*call scroller function here*/
                } else {
                        line_no++;
                }
        } else {
                column_no++;
        }


}
int print_to_console(const char *string, int size)
{
        int count = 0;
        while (*string && count < size) {
                *v_ptr = *string; /*sending output to video memory*/
                ++string;
                v_ptr +=2;
                count++;
                update_linefeed();
        }
        stat = 0;

        return 1;
}

/*calculate length of string*/
/*
int strlen(char *str)
{
        int len = 0;

        while(*str++)
                len++;

        return len;
}
*/

/*for rinting string*/
int print_string(char *str)
{
        int size = strlen(str);
        print_to_console(str,size);
        return 1;
}

/*for printing integer*/
int print_int(int num)
{
        char str[5], temp;
        int index = 0, j = 0;
        char *t = str;
        int n=5;
        while (n != 0) {
                *t++ = 0;
                n--;
        }
       if (num == 0){
                str[index] = '0';
        }
while (num > 0) {
                str[index++] = '0' + (num%10);
                num/=10;
        }

        for (j = 0; j < index/2; j++) {
                temp = str[j];
                str[j] = str[index-1-j];
                str[index-1-j] = temp;
        }

        int size = strlen(str);

        print_to_console((const char *)str,size);
        return 1;
}

/*for printing hex*/
int print_hex(int addr) {
        char arr[33];
        char *ptr = &arr[sizeof(arr)-1];
        *ptr = '\0';
       int count = 0;
        while( addr != 0 )
        {
                *--ptr="0123456789abcdef"[addr%16];
                addr/=16;
                count++;
        }

        print_to_console(ptr,count);
        return 1;
}

/*for printing pointer*/
int print_ptr(void *addr_main)
{
        print_string("0x");
        char arr[33];
        unsigned long long addr = (unsigned long long ) addr_main;
        char *ptr = &arr[sizeof(arr)-1];
        *ptr = '\0';
       int count = 0;
        while( addr != 0 )
        {
                *--ptr="0123456789abcdef"[addr%16];
                addr/=16;
                count++;
        }

print_to_console(ptr,count);
        return 1;

}


/*for printing char*/
int print_char(int ch) {
        char str[2];
        str[0] = ch;
        str[1] = '\0';
        int size = strlen(str);
        print_to_console(str,size);
        return 1;
}


/*for printing line at particular coordinate*/
int print_line(int row,int column,const char *string, ...) {


        char *ptr = v_ptr;
        v_ptr = starting_address_of_VGA_buffer+(2*80*column+row*2); 

        va_list val;
        va_start(val, string);


        vprintf(string,val);
        va_end(val);

        v_ptr = ptr;
        return 1;


}

/*for function of backspace*/
int check_back_space(const char *format) {
        char *sp =NULL;
        int x = 2;
        if (*format == '\b' ) {
                v_ptr= v_ptr-x;
                *v_ptr= *sp;

                if (column_no == 0) {
                         column_no = 79;
                        line_no--;
                         } else {
column_no--;
                        }

                return 1;
        } else {

                return 0;
        }

}


/*for clearing console*/
void clear_console() {
        v_ptr = (char *)0xFFFFFFFF800B8000;
        int count =0;
        while (count < (80*25*2)) {

        *(v_ptr+count) = 0;
        count++;
        }


}

/*for executing printf*/
static void vprintf(const char *format,va_list args)
{
        int printed = 0;
        const char *ch;
        //int size;
        int ret;
       while (*format != 0 ) {

                if (*format == '%') {
                        ch = format;
                        ++format;

                        if (*format == '\0'){
                                break;
                        }
                        switch(*format) {
                        case 's':
                                ret = print_string(va_arg(args, char *));
                                printed=printed+ret;
                                break;

                        case 'd':
                                print_int(va_arg(args, int));
 printed++;
                                break;

                        case 'c':
                                print_char(va_arg(args, int));
                                printed++;
                                break;

                        case 'x':
                                print_hex(va_arg(args, int));
                                printed++;
                                break;

                        case 'p':
                                print_ptr(va_arg(args,void *));
                                printed++;
                                break;

                        default :
                                print_to_console(ch,1);
                                print_to_console(format,1);
                                printed++;
                        }
                } else {
                        format = format + check_print_escape(format) + check_back_space(format);
                        print_to_console(format,1);
                }

                format++;
        }
}


/*printf implementation*/
void printf(const char *format, ...) {
        va_list val;
        va_start(val, format);
        vprintf(format,val);
        va_end(val);
}

/*function to print newline*/
void print_newline() {
                v_ptr = v_ptr+ (160 - 2*column_no); 
		column_no= 0;
                line_no++;
                if (line_no > 24);
                       //  clear_console(); calll scroller here
}


/*function to print backspace*/
void print_backspace() {
        v_ptr = v_ptr - 2;
        *v_ptr = ' ';
        v_ptr=v_ptr-2;


/*       // char *sp =NULL;
        //      *sp = ' ';
        int x=2;

        if (stat == 0) {
                char *c1;
        char *c2;
              c1 = (char *)(v_ptr-2);
        c2 = (char *)(v_ptr-4);
                *v_ptr = ' ';
                v_ptr= v_ptr - x;
        *v_ptr =' ';
        v_ptr = v_ptr - 2;
        print_to_console(c2,1);
                        print_to_console(c1,1);
                stat=1;
        } else {
                *v_ptr = ' ';
                v_ptr= v_ptr - x;
        *v_ptr =' ';
        v_ptr = v_ptr - 2;

        } */
                if (column_no == 0) {
                         column_no = 79;
                        line_no--;
                         } else {
                        column_no--;
                        }


}

int32_t puts(char* mystring)
{
//    char *temp = mystring;
    int32_t len = 0;
 /*   for (; *temp; temp++) {
        *v_ptr = *temp;
	v_ptr += 2;
        len++;
    }
//	v_ptr += 2; */
	printf(mystring);
    return len;
}
