#include <sys/defs.h>
#include <sys/pmap.h>
#include <sys/sbunix.h>
#include <sys/vm.h>

enum PAGE_FLAGS {

    PAGING_PRESENT         =   1UL,                                //00000000000000000000000000000000 00000000000000000000000000000001
    PAGING_WRITABLE        =   2UL,                                //00000000000000000000000000000000 00000000000000000000000000000010
    PAGING_USER            =   4UL,                                //00000000000000000000000000000000 00000000000000000000000000000100
    PAGING_PWT             =   8UL,                                //00000000000000000000000000000000 00000000000000000000000000001000
    PAGING_PCD             =   0x10UL,                             //00000000000000000000000000000000 00000000000000000000000000010000
    PAGING_ACCESSED        =   0x20UL,                             //00000000000000000000000000000000 00000000000000000000000000100000
    PAGING_IGN             =   0x40UL,                             //00000000000000000000000000000000 00000000000000000000000001000000
    PAGING_ZERO            =   0UL,                                //00000000000000000000000000000000 00000000000000000000000000000000
    PAGING_ING2            =   0x100UL,                            //00000000000000000000000000000000 00000000000000000000000100000000
    PAGING_MBZ_PML4        =   0x180UL,                            //00000000000000000000000000000000 00000000000000000000000110000000
    PAGING_AVL             =   0xD00UL,                            //00000000000000000000000000000000 00000000000000000000111000000000
    PAGING_ADDR            =   0x000FFFFFFFFFF000UL,               //00000000000011111111111111111111 11111111111111111111000000000000
    PAGING_AVAILABLE       =   0x3FF0000000000000UL,               //00111111111100000000000000000000 00000000000000000000000000000000
    PAGING_COW             =   0x4000000000000000UL,               //01000000000000000000000000000000 00000000000000000000000000000000
    PAGING_NX              =   0x8000000000000000UL,               //10000000000000000000000000000000 00000000000000000000000000000000
    PAGING_FLAGS           =   0xFFF0000000000FFFUL                //11111111111100000000000000000000 00000000000000000000111111111111
};

#define ENTRIES_PER_PTE  512
#define ENTRIES_PER_PDE  512
#define ENTRIES_PER_PDPE 512
#define ENTRIES_PER_PML4 512

#define PTE_SELF_REF  0xFFFFFF0000000000UL
#define PDE_SELF_REF  0xFFFFFF7F80000000UL
#define PDPE_SELF_REF 0xFFFFFF7FBFC00000UL
#define PML4_SELF_REF 0xFFFFFF7FBFDFE000UL

#define PAGESIZE 0x1000
#define PAGE_2ALIGN 12     // 2 ^ PAGE_2ALIGN = PAGESIZE
#define PAGE_ALIGN(ADDR) ((ADDR) >> 12 << 12)
#define KERNEL_START_VADDR 0xFFFFFFFF80000000UL

// For accessing Page table addresses, we need to first convert PhyADDR to VirADDR
// So need to add below kernel base address
#define VADDR(PADDR) ((KERNEL_START_VADDR) + PAGE_ALIGN(PADDR))
#define PADDR(VADDR) (PAGE_ALIGN(VADDR) - (KERNEL_START_VADDR))

#define RW_KERNEL_FLAGS (PAGING_PRESENT | PAGING_WRITABLE)
#define IS_PRESENT_PAGE(entry)  ((entry) & PAGING_PRESENT)

uint64_t ker_cr3;
uint64_t *ker_pml4_t;
extern char kernmem;

static uint64_t* alloc_pte(uint64_t *pde_table, int pde_off)
{
    uint64_t pte_table = (uint64_t)allocate_page();
    pde_table[pde_off] = pte_table | RW_KERNEL_FLAGS;   
    return (uint64_t*)VADDR(pte_table);
} 

static uint64_t* alloc_pde(uint64_t *pdpe_table, int pdpe_off)
{
    uint64_t pde_table = (uint64_t)allocate_page();
    pdpe_table[pdpe_off] = pde_table | RW_KERNEL_FLAGS;   
    return (uint64_t*)VADDR(pde_table);
}

static uint64_t* alloc_pdpe(uint64_t *pml4_table, int pml4_off)
{
    uint64_t pdpe_table = (uint64_t)allocate_page();
    pml4_table[pml4_off] = pdpe_table | RW_KERNEL_FLAGS;   
    return (uint64_t*)VADDR(pdpe_table);
}
void map_kernel(uint64_t vaddr, uint64_t paddr, uint64_t no_of_pages)
{

	uint64_t *pdpe_table = NULL, *pde_table = NULL, *pte_table = NULL;

    int i, j, k, phys_addr, pde_off, pdpe_off, pml4_off , pte_off;

    pte_off  = (vaddr >> 12) & 0x1FF;
    pde_off  = (vaddr >> 21) & 0x1FF;
    pdpe_off = (vaddr >> 30) & 0x1FF;
    pml4_off = (vaddr >> 39) & 0x1FF;

    // kprintf(" $$ NEW MAPPING $$ OFF => %d %d %d %d ", pml4_off, pdpe_off, pde_off, pte_off);

    phys_addr = (uint64_t) *(ker_pml4_t + pml4_off);
    // kprintf("%p",phys_addr);
    if (IS_PRESENT_PAGE(phys_addr)) {
        pdpe_table =(uint64_t*) VADDR(phys_addr); 

        phys_addr = (uint64_t) *(pdpe_table + pdpe_off);
        if (IS_PRESENT_PAGE(phys_addr)) {
            pde_table =(uint64_t*) VADDR(phys_addr); 

            phys_addr  = (uint64_t) *(pde_table + pde_off);
            if (IS_PRESENT_PAGE(phys_addr)) {
                pte_table =(uint64_t*) VADDR(phys_addr); 
            } else {
                pte_table = alloc_pte(pde_table, pde_off);
            }
        } else {
            pde_table = alloc_pde(pdpe_table, pdpe_off);
            pte_table = alloc_pte(pde_table, pde_off);
        }
    } else {
        pdpe_table = alloc_pdpe(ker_pml4_t, pml4_off);
        pde_table = alloc_pde(pdpe_table, pdpe_off);
        pte_table = alloc_pte(pde_table, pde_off);
    }

    phys_addr = paddr;  

    if (no_of_pages + pte_off <= ENTRIES_PER_PTE) {
        for (i = pte_off; i < (pte_off + no_of_pages); i++) {
            pte_table[i] = phys_addr | RW_KERNEL_FLAGS; 
            phys_addr += PAGESIZE;
        }
        // kprintf(" SIZE = %d PDPE Address %p, PDE Address %p, PTE Address %p", no_of_pages, pdpe_table , pde_table, pte_table);
    } else {
        int lno_of_pages = no_of_pages, no_of_pte_t;

        // kprintf(" SIZE = %d PDPE Address %p, PDE Address %p, PTE Address %p", lno_of_pages, pdpe_table ,pde_table, pte_table);
        for ( i = pte_off ; i < ENTRIES_PER_PTE; i++) {
            pte_table[i] = phys_addr | RW_KERNEL_FLAGS;
            phys_addr += PAGESIZE;
        }
        lno_of_pages = lno_of_pages - (ENTRIES_PER_PTE - pte_off);
        no_of_pte_t = lno_of_pages/ENTRIES_PER_PTE;

        for (j = 1; j <= no_of_pte_t; j++) {   
            pte_table = alloc_pte(pde_table, pde_off+j);
            // kprintf(" SIZE = %d PDPE Address %p, PDE Address %p, PTE Address %p", lno_of_pages, pdpe_table ,pde_table, pte_table);
            for(k = 0; k < ENTRIES_PER_PTE; k++ ) { 
                pte_table[k] = phys_addr | RW_KERNEL_FLAGS;
                phys_addr += PAGESIZE;
            }
        }
        lno_of_pages = lno_of_pages - (ENTRIES_PER_PTE * pte_off);
        pte_table = alloc_pte(pde_table, pde_off+j);
        
        // kprintf(" SIZE = %d PDPE Address %p, PDE Address %p, PTE Address %p", lno_of_pages, pdpe_table ,pde_table, pte_table);
        for(k = 0; k < lno_of_pages; k++ ) { 
            pte_table[k] = phys_addr | RW_KERNEL_FLAGS;
            phys_addr += PAGESIZE;
        }
    }	
}

 void setup_page_tables()
{
	uint64_t physbase = (uint64_t)get_physbase();
	uint64_t vaddr = (uint64_t)&kernmem; 
	int pages = 108;
	
	ker_cr3 = (uint64_t)allocate_page();

	ker_pml4_t = (uint64_t*) VADDR(ker_cr3);
	ker_pml4_t[510] = ker_cr3 | RW_KERNEL_FLAGS;

	map_kernel(vaddr, physbase, pages);
	map_kernel(0xFFFFFFFF800B8000, 0xB8000, 1);

	
}
void load_CR3()
{

	uint64_t cr3 = ker_cr3; 
	__asm volatile("movq %0, %%cr3":: "b"(cr3));
}
