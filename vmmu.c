#include <sys/defs.h>
#include <sys/sbunix.h>
#include <sys/pmap.h>
#include <sys/vmmu.h>
#include <sys/string.h>

#define VIDEO_MEMORY	0xb8000UL
#define PAGE_SIZE	0x1000UL

#define FRAME 0xFFFFFFFFFFFFF000

uint64_t get_address(uint64_t* entry)
{
    return (*entry & FRAME);
}

extern char kernmem;
extern uint64_t *mmap;
extern char *starting_address_of_VGA_buffer;

struct PML4 *pml4;
uint64_t cr3;
uint64_t lastVptr = IDENTITY_MAP_VSTART;

void load_CR3() {

	uint64_t base_pgdir_addr = (uint64_t)pml4;
	__asm volatile("movq %0, %%cr3":: "b"(base_pgdir_addr));	
}


uint64_t get_CR3()
{
	uint64_t saved_cr3;
	__asm volatile("mov %%cr3, %0" : "=r" (saved_cr3));

	return saved_cr3;
}

void set_CR3(struct PML4 *new_cr3)
{
	uint64_t base_pgdir_addr = (uint64_t)new_cr3;
	__asm volatile("mov %0, %%cr3":: "b"(base_pgdir_addr));
}

void* pdpt_alloc(uint64_t vAddress)
{
	struct PDPT *pdpt = (struct PDPT *)allocate_page();
	uint64_t pdpt_entry = (uint64_t)pdpt;
	pdpt_entry |= (PTE_P|PTE_W|PTE_U);
	pml4->pgtable_entries[PML4_INDEX((uint64_t)vAddress)] = pdpt_entry;

	return (void *)pdpt;
}

void* pdt_alloc(struct PDPT *pdpt, uint64_t vAddress)
{
	struct PDT *pdt = (struct PDT*)allocate_page();
        uint64_t pdt_entry = (uint64_t)pdt;
        pdt_entry |= (PTE_P|PTE_W|PTE_U);
        pdpt->pgtable_entries[PDPT_INDEX((uint64_t)vAddress)] = pdt_entry;

	return (void *)pdt;
}

void* pt_alloc(struct PDT *pdt, uint64_t vAddress)
{
	struct PT *pt = (struct PT *)allocate_page();
        uint64_t pt_entry = (uint64_t)pt;
        pt_entry |= (PTE_P|PTE_W|PTE_U);
        pdt->pgtable_entries[PDT_INDEX((uint64_t)vAddress)] = pt_entry;

	return (void *)pt;
} 

void setup_page_tables(uint64_t base) {
	struct PDPT *pdpt;
	struct PDT *pdt;
	struct PT *pt;
	uint64_t vAddress = (uint64_t)&kernmem;
	uint64_t physfree = get_physfree();
	uint64_t physbase = base;

	pml4 = (struct PML4 *)allocate_page();
	if(!pml4)
		return;

	pdpt = pdpt_alloc(vAddress);
	if(!pdpt)
		return;
	
	pdt = pdt_alloc(pdpt, vAddress);
	if(!pdt)
		return;

	pt = pt_alloc(pdt, vAddress);
	if(!pt)
		return;

	for(; physbase<physfree ;  physbase += 0x1000, vAddress += 0x1000) {

		uint64_t entry = physbase;
		entry |= (PTE_P|PTE_W|PTE_U);
		pt->pgtable_entries[PT_INDEX(vAddress)] = entry;
	}

}

static void
map_virt_phys_addr(
	uint64_t vaddr,
	uint64_t paddr)
{
	struct PDPT	*pdpt;
	struct PDT	*pdt;
	struct PT	*pt;

	uint64_t pml4_entry = pml4->pgtable_entries[PML4_INDEX((uint64_t)vaddr)];
	if(pml4_entry & PTE_P)
		pdpt = (struct PDPT *)get_address(&pml4_entry);
	else
		pdpt = (struct PDPT*)pdpt_alloc(vaddr);

	uint64_t pdpt_entry = pdpt->pgtable_entries[PDPT_INDEX((uint64_t)vaddr)];
	if(pdpt_entry & PTE_P)
		pdt = (struct PDT*)get_address(&pdpt_entry);
	else
		pdt = (struct PDT*)pdt_alloc(pdpt, vaddr);

	uint64_t pdt_entry = pdt->pgtable_entries[PDT_INDEX((uint64_t)vaddr)];
	if(pdt_entry & PTE_P)
                pt = (struct PT*)get_address(&pdt_entry);
        else
		pt = (struct PT*)pt_alloc(pdt, vaddr);

	uint64_t entry = paddr;
	entry |= (PTE_P|PTE_W|PTE_U);
	pt->pgtable_entries[PT_INDEX((uint64_t)vaddr)] = entry;

}


void set_identity_paging() {

	uint64_t vaddr = IDENTITY_MAP_VSTART;
	uint64_t paddr = IDENTITY_MAP_PSTART;
	uint64_t max_phys = get_maxphysfree();

	for(; paddr <= max_phys; paddr += PAGE_SIZE, vaddr += PAGE_SIZE){
		map_virt_phys_addr(vaddr, paddr);
	}

	map_virt_phys_addr((uint64_t)0xffffffff800b8000, VIDEO_MEMORY);
	starting_address_of_VGA_buffer = (char *)0xffffffff800b8000;

	mmap = (uint64_t*)(0xFFFFFFFF80000000UL | (uint64_t) mmap); 
}

uint64_t get_pml4_entry(struct PML4 *pml4, uint64_t vaddr)
{
	pml4 = (struct PML4 *)((uint64_t)pml4 | IDENTITY_MAP_VSTART);

	uint64_t pml4_entry = pml4->pgtable_entries[PML4_INDEX((uint64_t)vaddr)];
/*
	uint64_t pml4_entry = ((struct PML4 *) ((uint64_t) pml4
			IDENTITY_MAP_VSTART))->pgtable_entries[PML4_INDEX(
			(uint64_t)vaddr)];
*/
	return pml4_entry;
}

uint64_t get_pdpt_entry(struct PDPT* pdpt, uint64_t vaddr)
{
	pdpt = (struct PDPT *)((uint64_t)pdpt | IDENTITY_MAP_VSTART);
	uint64_t pdpt_entry = pdpt->pgtable_entries[PDPT_INDEX((uint64_t )vaddr);

	return pdpt_entry;	
}

uint64_t get_pdt_entry(struct PDT *pdt, uint64_t vaddr)
{
	pdt = (struct PDT *) ((uint64_t) pdt | IDENTITY_MAP_VSTART);
	uint64_t pdt_entry = pdt->pgtable_entries[PDT_INDEX((uint64_t )vaddr)];

	return pdt_entry;
}

uint64_t get_pt_entry(struct PT *pt, uint64_t vaddr)
{
	pt = (struct PT *)((uint64_t) pt | IDENTITY_MAP_VSTART);
	uint64_t pt_entry = pt->pgtable_entries[PT_INDEX((uint64_t )vaddr);

	return pt_entry;
}

void
map_process(
        uint64_t vaddr,
        uint64_t paddr)
{
        struct PDPT     *pdpt;
        struct PDT      *pdt;
        struct PT       *pt;

	pml4 = (struct PML4*) get_CR3();
	pml4 = (struct PML4*) (IDENTITY_MAP_VSTART | (uint64_t) pml4); 
        uint64_t pml4_entry = pml4->pgtable_entries[PML4_INDEX((uint64_t)vaddr)];

        if(pml4_entry & PTE_P)
                pdpt = (struct PDPT *)get_address(&pml4_entry);
        else
		pdpt = (struct PDPT*)pdpt_alloc(vaddr); 

/*
	pdpt = (struct PDPT*)((uint64_t) pdpt | IDENTITY_MAP_VSTART);
        uint64_t pdpt_entry = pdpt->pgtable_entries[PDPT_INDEX((uint64_t)vaddr)];
*/

	uint64_t pdpt_entry = get_pdpt_entry(pdpt, vaddr);
        if(pdpt_entry & PTE_P)
                pdt = (struct PDT*)get_address(&pdpt_entry);
        else
		pdt = (struct PDT*)pdt_alloc(pdpt, vaddr);

/*
	pdt = (struct PDT*)((uint64_t) pdt | IDENTITY_MAP_VSTART); 
        uint64_t pdt_entry = pdt->pgtable_entries[PDT_INDEX((uint64_t)vaddr)];
*/

	uint64_t pdt_entry = get_pdt_entry(pdt, vaddr);
        if(pdt_entry & PTE_P)
                pt = (struct PT*)get_address(&pdt_entry);
        else
		pt = (struct PT*)pt_alloc(pdt, vaddr);


	pt = (struct PT*)((uint64_t) pt | IDENTITY_MAP_VSTART);
        uint64_t entry = paddr;
        entry |= (PTE_P|PTE_W|PTE_U);

        pt->pgtable_entries[PT_INDEX((uint64_t)vaddr)] = entry;

}


void* kmalloc()
{
	uint64_t page = (uint64_t)allocate_page();
	struct PML4 *newPML4 = (struct PML4 *)get_CR3();

	pml4 = newPML4;

	set_CR3(pml4);

	lastVptr += 0x1000;
	map_process(lastVptr, page);
	memset((void *)lastVptr, 0, 4096);


	return (void *)(lastVptr);
}

void* set_user_AddrSpace()
{
	struct PML4 *newPML4 = (struct PML4 *)allocate_page();
	struct PML4 *curPML4 = (struct PML4 *)get_CR3();

	((struct PML4 *)((uint64_t)newPML4 | IDENTITY_MAP_VSTART))->pgtable_entries[511] =
		((struct PML4 *)((uint64_t)curPML4 | IDENTITY_MAP_VSTART))->pgtable_entries[511];

	return (void *)newPML4;
}

/*
void *kmalloc_new(uint32_t size)
{
	void *ret = NULL;
	int npages = 0;

	if(size%16) {
		size = size >> 4 << 4;
		size += 16;
	}

	if(size > ) {
	} 
}
*/

void setup_child_pagetable(uint64_t child_PML4)
{
	struct PML4 *c_pml4 = (struct PML4 *)child_PML4;
	struct PML4 *p_pml4 = (struct PML4 *)allocate_page();

	int pml4_indx = 0;
	for(; pml4_indx < 510; pml4_indx++) {
		uint64_t pml4_entry =
			((struct PML4 *)((uint64_t)p_pml4 | IDENTITY_MAP_VSTART))->pgtable_entries[pml4_indx];

		if(pml4_entry & PTE_P) {
			struct PDPT *c_pdpt = (struct PDPT *)allocate_page();
			uint64_t c_pdpt_entry = (uint64_t) c_pdpt;
			c_pdpt_entry |= PTE_P|PTE_W|PTE_U;	

			((struct PML4 *)((uint64_t)c_pml4 | IDENTITY_MAP_VSTART))->pgtable_entries[pml4_indx] = 
						(uint64_t) c_pdpt_entry;

			struct PDPT *p_pdpt = (struct PDPT *) get_address(&pml4_entry);

			int pdpt_indx = 0;
			for(; pdpt_indx < 512; pdpt_indx++) {
				uint64_t pdpt_entry = ((struct PDPT *) ((uint64_t)p_pdpt |
						IDENTITY_MAP_VSTART))->pgtable_entries[pdpt_indx];

				if(pdpt_entry & PTE_P) {
					struct PDT *c_pdt = (struct PDT *)allocate_page();
					uint64_t c_pdt_entry = (uint64_t) c_pdt;
					c_pdt_entry |= PTE_P|PTE_W|PTE_U;

					((struct PDPT *) ((uint64_t)p_pdpt |
						IDENTITY_MAP_VSTART))->pgtable_entries[pdpt_indx] = 
						(uint64_t)c_pdt_entry;

					struct PDT *p_pdt = (struct PDT *) get_address(&pdpt_entry);

					int pdt_indx = 0;
					for(; pdt_indx < 512; pdt_indx++) {
						uint64_t pdt_entry = ((struct PDT *)((uint64_t)p_pdt |
							IDENTITY_MAP_VSTART))->pgtable_entries[pdt_indx];
						if(pdt_entry & PTE_P) {
							struct PT *c_pt = (struct PT *)allocate_page();
							uint64_t c_pt_entry = (uint64_t) c_pt;
							c_pt_entry |= PTE_P|PTE_W|PTE_U;
							
							((struct PDT *)((uint64_t)c_pdt |
								IDENTITY_MAP_VSTART))->pgtable_entries[pdt_indx]
								= (uint64_t) c_pt_entry;

							struct PT *p_pt = (struct PT *)get_address(&pdt_entry);

							int pt_indx = 0;
							for(; pt_indx < 512; pt_indx++) {
								uint64_t pt_entry = ((struct PT *)((uint64_t)p_pt |
									IDENTITY_MAP_VSTART))->pgtable_entries[pt_indx];
								if(pt_entry & PTE_P) {
									pt_entry = ((uint64_t)get_address(&pt_entry))|
											PTE_P | PTE_U | PTE_COW; 
									((struct PT *)((uint64_t)c_pt |
										IDENTITY_MAP_VSTART))->pgtable_entries[pt_indx]
										= (uint64_t)pt_entry;
									
								}	
							}	
						}		
					}		 
				}	
			}	
		}
	}

	((struct PML4 *)((uint64_t)c_pml4 | IDENTITY_MAP_VSTART))->pgtable_entries[511] = 
		((struct PML4 *)((uint64_t)p_pml4 | IDENTITY_MAP_VSTART))->pgtable_entries[511];

	((struct PML4 *)((uint64_t)c_pml4 | IDENTITY_MAP_VSTART))->pgtable_entries[510] =
		((struct PML4 *)((uint64_t)p_pml4 | IDENTITY_MAP_VSTART))->pgtable_entries[510]; 
}


void kfree(uint64_t vaddr)
{
	struct PDPT *pdpt = NULL;
	struct PDT *pdt = NULL;
	struct PT *pt = NULL;
	uint64_t paddr = NULL;

	struct PML4 *pml4 = (struct PML4 *) get_CR3();
	uint64_t pml4_entry = get_pml4_entry(pml4, vaddr);

	if(pml4_entry & PTE_P)
		pdpt = (struct PDPT *)get_address(&pml4_entry);

	uint64_t pdpt_entry = get_pdpt_entry(pdpt, vaddr);
	if(pdpt_entry & PTE_P)
		pdt = (struct PDT *)get_address(&pdpt_entry);

	uint64_t pd_entry = get_pdt_entry(pdt, vaddr);
	if(pdt_entry & PTE_P)
		pt = (struct PT *)get_address(&pdt_entry);

	if(pt != NULL)
		paddr = get_pt_entry(pt, vaddr); 

	deallocate_page((void *)paddr); 
} 
