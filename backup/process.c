#include <sys/vmmu.h>
#include <sys/process.h>
#include <sys/gdt.h>
#include <sys/string.h>
#include <sys/sbunix.h>
#include <sys/elf.h>
#include <sys/tarfs.h>
#include <sys/pmap.h>
#include <sys/syscall.h>

task_struct *current;

uint64_t curStack;
uint64_t userStackAddr = 0xFFFFFFFF02000000;

extern struct isr_regs *parent;
 
int getProcessID()
{
	int id = 0;
	for(;id < MAX_PROCESS; id++) {
		if(processID[id] == 0) {
			processID[id] = 1;
			return id;
		}
	}
	return -1;
}


void init_process_map()
{
	int id = 0;
	for(; id < MAX_PROCESS; id++)
		processID[id] = 0;
}


void func1(){
	printf("barney");

	while(1)
		schedule();
}


void create_init_process()
{
	/* init process task struct */
	task_struct *init = (task_struct *) kmalloc();

	init->task_state = TASK_NEW;
	init->pid = getProcessID();
	init->ppid = 0;
	init->next = NULL;
	init->mm = NULL;
	init->sleep_time = 0;
	init->wait_on_child_pid = -1;
	init->cr3 = (uint64_t)get_CR3();
	init->rip = (uint64_t)&func1;

	void *kstack = (void *)kmalloc();
	init->rsp = init->init_stack = init->kernel_stack = (uint64_t)kstack;

	current = init;

}

task_struct* create_user_process(char *filename)
{
	/* read the elf header from the provided binary */ 
	struct posix_header_ustar *file = get_binary(filename);

	if(file == NULL) {
		printf("Binary not found!\n");
		return NULL;
	}

	int pid = getProcessID();

	/* new user process */
	task_struct *newProc = (task_struct *)kmalloc();
	newProc->pid = pid;
	newProc->task_state = TASK_NEW;
	newProc->wait_on_child_pid = -1;
	
	newProc->cr3 = (uint64_t)set_user_AddrSpace();
	struct PML4 *curr_CR3 = (struct PML4 *)get_CR3();

	/* set the current CR3 value to new process pml4 address */
	set_CR3((struct PML4 *)newProc->cr3);


	/* allocate space to mm structure */ 
	mm_struct *mm = (mm_struct *)kmalloc();
	newProc->mm = mm;

	/* kernel stack, set rsp to the base address of the stack */ 
	void *kStack = (void *)kmalloc();
	newProc->rsp = newProc->kernel_stack = (uint64_t)kStack;

	/* load the elf */
	int error = load_exe(newProc, (void *)(file + 1));

	if(error)
		return NULL;


	/* set the CR3 value back to the prev process pml4 */ 
	set_CR3((struct PML4 *)curr_CR3);

	/* make the new process as the current process */
	current->next = newProc;
	newProc->next = current;
	current = newProc; 

	return newProc;
}

void switch_to_ring3(task_struct *task)
{
	task->task_state = TASK_RUNNING;

	int a = 0x2B;
	__asm volatile("movq %0,%%rax;\n\t"
            "ltr (%%rax);"::"r"(&a));

	tss.rsp0 = task->kernel_stack;

	uint64_t *stack = (uint64_t *)get_phy_addr(userStackAddr);
	task->rsp = (uint64_t)&stack[500]; 
	
	__asm__ __volatile__ (
	"sti;"
        "movq %0, %%cr3;"
        "mov $0x23, %%ax;"
        "mov %%ax, %%ds;"
        "mov %%ax, %%es;"
        "mov %%ax, %%fs;"
        "mov %%ax, %%gs;"

        "movq %1, %%rax;"
        "pushq $0x23;"
        "pushq %%rax;"
        "pushfq;"
        "popq %%rax;"
        "orq $0x200, %%rax;"
        "pushq %%rax;"
        "pushq $0x1B;"
        "pushq %2;"
	"movq $0x0, %%rdi;"
	"movq $0x0, %%rsi;" 
        "iretq;"
        ::"r"(task->cr3), "r"((uint64_t)(&stack[500])),"r"(task->rip)
    );

}

void* copy_task_struct(task_struct *parent)
{
	task_struct *child_task = (task_struct *)kmalloc();

	child_task->task_state = TASK_NEW;
	child_task->pid = getProcessID();
	child_task->ppid = parent->pid ;
	child_task->next = NULL;
	child_task->mm = NULL;
	child_task->sleep_time = 0;
        child_task->wait_on_child_pid = -1;

	/* set child page tables */
	child_task->cr3 = (uint64_t)allocate_page();
	setup_child_pagetable(child_task->cr3);

	set_CR3((struct PML4 *)child_task->cr3);


	/* copy Kernel stack */
	void *kstack = (void *)kmalloc(); 
        child_task->kernel_stack = (uint64_t)kstack;
	memcpy((void *)child_task->kernel_stack, (void *)current->kernel_stack, 4096);


	/* copy file descriptors */
	memcpy(&(child_task->fd[0]),&(parent->fd[0]),(sizeof(parent->fd[0])* MAX_FD));

	/* copy mm_struct */ 
	child_task->mm = kmalloc();
	memcpy(child_task->mm, parent->mm, sizeof(mm_struct));

	/* copy vmas */
	vma_struct *parent_vma = parent->mm->mmap;
	vma_struct *child_vma = NULL;

	while(parent_vma) {

		child_vma = (vma_struct *)kmalloc();

		memcpy(child_vma, parent_vma, sizeof(vma_struct));

		if(parent_vma->file!=NULL){
			child_vma->file = kmalloc();
			memcpy(child_vma->file,parent_vma->file,sizeof(struct file));
		}

		if(child_vma->next)
			child_vma = child_vma->next;
		parent_vma = parent_vma->next;
	}

	set_CR3((struct PML4 *)parent->cr3);	

	if(!child_vma) {
		child_task->mm->mmap = NULL;
		goto ret;
	}		

	child_vma->next = NULL;


ret:
	return (void *)child_task;

}

int sys_fork()
{

	/* copy parent's task_struct to Child task structure */
	task_struct *child = (task_struct *)copy_task_struct((task_struct*)current);
	task_struct *par_tsk = (task_struct*)current;
	uint64_t ps;


	/* queue the child task just after the parent(current) task */
	task_struct *temp = current->next;
	current->next = child;
	child->next = temp;


	__asm__ __volatile__ (
        "movq %%rsp, %1;"           //copy RSP
        "movq $2f, %0;"             // restore RIP so as to continue execution in next task
        "2:\t"
        :"=g"(child->rip),"=m"(ps)
    );

	if(par_tsk == current) {
	child->kernel_stack = child->kernel_stack - (current->kernel_stack - ps);
return child->pid;
} else
{
	outb(0x20, 0x20);
	return 0;
}
/*
	__asm volatile("movq %%rsp, %0;":"=g"(curStack));
	__asm volatile( "movq %0, %%rsp ": : "m"(child->rsp) : "memory" );

	__asm volatile("pushq $0x23\n\t"
            "pushq %0\n\t"
            "pushq $0x200\n\t"
            "pushq $0x1b\n\t"
            "pushq %1\n\t"
            : :"m"(parent->rsp),"m"(parent->rip) :"memory");
    __asm volatile(
	    "pushq %0\n\t"
            "pushq %1\n\t"
            "pushq %2\n\t"
            "pushq %3\n\t"
            "pushq %4\n\t"
            "pushq %5\n\t"
            "pushq %6\n\t"
            "pushq %7\n\t"
            "pushq %8\n\t"
            "pushq %9\n\t"
            "pushq %10\n\t"
            "pushq %11\n\t"
            "pushq %12\n\t"
            "pushq %13\n\t"
            "pushq %14\n\t"
            : :"m"(parent->rax), "m"(parent->rbx),"m"(parent->rcx),"m"(parent->rdx),
		"m"(parent->rbp),"m"(parent->rdi),"m"(parent->rsi), "m"(parent->r8),
		"m"(parent->r9),"m"(parent->r10),"m"(parent->r11), "m"(parent->r12),
		"m"(parent->r13), "m"(parent->r14), "m"(parent->r15):"memory");
    __asm volatile("movq %%rsp, %0;":"=g"(child->rsp) : :"memory");
    __asm volatile( "movq %0, %%rsp ": : "m"(curStack) : "memory" );
*/
		return 0;

}

void switch_to(task_struct *next, task_struct *prev) {

	tss.rsp0 = next->kernel_stack;

	__asm__ __volatile__ (
	"sti;"
        "movq %%rsp, (%1);"     // save RSP
        "movq $1f, %0;"         // save RIP
        "movq %2, %%rsp;"       // load next->kernel_stack in rsp
        "movq %4, %%cr3;"       // load next->cr3 into cr3
        "pushq %3;"             // restore RIP so as to continue execution in next task
        "retq;"                 // Switch to new task
        "1:\t"
        :"=g"(prev->rip)
        :"r"(&(prev->kernel_stack)), "r"(next->kernel_stack), "r"(next->rip), "r"(next->cr3)
    );
}



void schedule()
{
	task_struct *prev =  (task_struct*)current;

	if(current->next) {
		current = current->next;
		if(current != prev)
			switch_to( (struct task_struct*)current, prev);
	}
}


/*
void schedule()
{
	task_struct *task = (task_struct*)current->next;

		
	if (task->task_state == TASK_NEW && task->pid != 0)
        {
            task->task_state = TASK_RUNNING;
            __asm volatile("movq %0, %%cr3":: "a"(task->cr3));
            __asm__ __volatile__("movq %%rsp,%0;":"=g"(current->rsp));
            current = task;
            __asm volatile( "movq %0, %%rsp ": : "m"(task->rsp) : "memory" );
            __asm__ __volatile__("movq %%rsp,%0;":"=g"(tss.rsp0));
            __asm volatile(
		"popq %%r15\n\t"
		"popq %%r14\n\t"
		"popq %%r13\n\t"
		"popq %%r12\n\t"
		"popq %%r11\n\t"
		"popq %%r10\n\t"
              	"popq %%r9\n\t"
                "popq %%r8\n\t"
                "popq %%rsi\n\t"
                "popq %%rdi\n\t"
		"popq %%rbp\n\t"
                "popq %%rdx\n\t"
                "popq %%rcx\n\t"
                "popq %%rbx\n\t"
		"popq %%rax\n\t"
              : : :"memory");
            __asm volatile("iretq":::"memory");
        } else {

		switch_to(current, task);
	}

}
*/


void free_memory_map(mm_struct *mm)
{
	if(mm->mmap == NULL)
		return;

	vma_struct *vma = mm->mmap;
	while(vma != NULL) {
		vma_struct *free_vma = vma;
		vma = vma->next;
		kfree((uint64_t)free_vma);
	}	
	
}

int execvpe(char *filename, char **argv, char **envp)
{
	/*free memory allocated to vmas */ 
	free_memory_map(current->mm);

	kfree((uint64_t)current->mm);
	current->mm = NULL;

	/* read the elf header from the provided binary */
        struct posix_header_ustar *file = get_binary(filename);

	if(file == NULL) {
                printf("Binary not found!\n");
                return -1;
        }

	/* allocate space to mm structure */
        mm_struct *mm = (mm_struct *)kmalloc();
        current->mm = mm;

	/* load the elf */
        int error = load_exe(current, (void *)(file + 1));

	if(error)
		return -1;

	return 0;

} 
