#include <sys/defs.h>
#include <sys/sbunix.h>
#include <sys/pmap.h>
#include <sys/vmmu.h>
#include <sys/string.h>

#define VIDEO_MEMORY	0xb8000UL
#define PAGE_SIZE	0x1000UL

#define FRAME		0xFFFFFFFFFFFFF000
#define KERN_BASE	0xFFFFFFFF80000000

#define PAGE_ALIGN(ADDR) ((ADDR) >> 12 << 12)

#define VADDR(PADDR) ((KERN_BASE) + PAGE_ALIGN(PADDR))
#define PADDR(VADDR) (PAGE_ALIGN(VADDR) - (KERN_BASE))


uint64_t get_address(uint64_t* entry)
{
    return (*entry & FRAME);
}

extern char kernmem;
extern uint64_t *mmap;
extern char *starting_address_of_VGA_buffer;

uint64_t boot_cr3;
uint64_t *pml4;
uint64_t cr3;
uint64_t kern_top_vaddr;

void load_CR3() {

	uint64_t base_pgdir_addr = (uint64_t)boot_cr3;
	__asm volatile("movq %0, %%cr3":: "b"(base_pgdir_addr));	
}


uint64_t get_CR3()
{
	uint64_t saved_cr3;
	__asm volatile("mov %%cr3, %0" : "=r" (saved_cr3));

	return saved_cr3;
}

void set_CR3(uint64_t *new_cr3)
{
	uint64_t base_pgdir_addr = (uint64_t)new_cr3;
	__asm volatile("mov %0, %%cr3":: "b"(base_pgdir_addr));
}

void* pdpt_alloc(uint64_t *pml4, uint64_t pml4_indx)
{
	uint64_t pdpt = (uint64_t)allocate_page();
	pdpt |= (PTE_P|PTE_W|PTE_U);
	pml4[pml4_indx] = pdpt;

	return (uint64_t *)VADDR(pdpt);
}

void* pdt_alloc(uint64_t *pdpt, uint64_t pdpt_indx)
{
	uint64_t pdt = (uint64_t)allocate_page();
        pdt |= (PTE_P|PTE_W|PTE_U);
        pdpt[pdpt_indx] = pdt;

	return (uint64_t *)VADDR(pdt);
}

void* pt_alloc(uint64_t *pdt, uint64_t pdt_indx)
{
	uint64_t pt = (uint64_t)allocate_page();
        pt |= (PTE_P|PTE_W|PTE_U);
        pdt[pdt_indx] = pt;

	return (uint64_t *)VADDR(pt);
} 

void setup_page_tables(uint64_t base) {
	uint64_t *pdpt;
	uint64_t *pdt;
	uint64_t *pt;
	uint64_t vAddress = (uint64_t)&kernmem;
	uint64_t physfree = get_physfree();
	uint64_t physbase = base;

	int pml4_indx = PML4_INDEX((uint64_t)vAddress);
        int pdpt_indx = PDPT_INDEX((uint64_t)vAddress);
        int pdt_indx = PDT_INDEX((uint64_t)vAddress);

	if(!boot_cr3) {
		boot_cr3 = (uint64_t)allocate_page(); 
		pml4 = (uint64_t *)VADDR(boot_cr3);
		pml4[510] = (boot_cr3 | PTE_P |PTE_W);
	}

	pdpt = pdpt_alloc(pml4, pml4_indx);
	pdt = pdt_alloc(pdpt, pdpt_indx);
	pt = pt_alloc(pdt, pdt_indx);

	for(; physbase<physfree ;  physbase += 0x1000, vAddress += 0x1000) {

        	int pt_indx = PT_INDEX((uint64_t)vAddress);
		uint64_t entry = physbase;
		entry |= (PTE_P|PTE_W|PTE_U);
		pt[pt_indx] = entry;
	}

	kern_top_vaddr = vAddress;

}


static void
map_virt_phys_addr(
	uint64_t vaddr,
	uint64_t paddr)
{
	uint64_t	*pdpt;
	uint64_t	*pdt;
	uint64_t	*pt;

	int pml4_indx = PML4_INDEX((uint64_t)vaddr);
	int pdpt_indx = PDPT_INDEX((uint64_t)vaddr);
	int pdt_indx = PDT_INDEX((uint64_t)vaddr);
	int pt_indx = PT_INDEX((uint64_t)vaddr);

	uint64_t pml4_entry = pml4[pml4_indx];
	if(pml4_entry & PTE_P)
		pdpt = (uint64_t *)get_address(&pml4_entry);
	else
		pdpt = (uint64_t*)pdpt_alloc(pml4, pml4_indx);

	uint64_t pdpt_entry = pdpt[pdpt_indx];
	if(pdpt_entry & PTE_P)
		pdt = (uint64_t*)get_address(&pdpt_entry);
	else
		pdt = (uint64_t*)pdt_alloc(pdpt, pdpt_indx);

	uint64_t pdt_entry = pdt[pdt_indx];
	if(pdt_entry & PTE_P)
                pt = (uint64_t*)get_address(&pdt_entry);
        else
		pt = (uint64_t*)pt_alloc(pdt, pdt_indx);

	uint64_t entry = paddr;
	entry |= (PTE_P|PTE_W|PTE_U);
	pt[pt_indx] = entry;

}

void set_identity_paging() {

	map_virt_phys_addr((uint64_t)0xFFFFFFFF800B8000, VIDEO_MEMORY);
	starting_address_of_VGA_buffer = (char *)0xFFFFFFFF800B8000;
	recalculate_offset(starting_address_of_VGA_buffer);

	mmap = (uint64_t*)(0xFFFFFFFF80000000UL | (uint64_t) mmap); 
}

/*
void* kmalloc()
{

	uint64_t page = (uint64_t)allocate_page();
	uint64_t *newPML4 = (uint64_t *)get_CR3();

	pml4 = newPML4;

	set_CR3(pml4);

	lastVptr += 0x1000;
	map_process(lastVptr, page);
	memset((void *)lastVptr, 0, 4096);


	return (void *)(lastVptr);
}
*/

uint64_t* get_pte_entry(uint64_t vaddr)
{
    uint64_t tvaddr;
    uint64_t *addr;

    tvaddr  = vaddr << 16 >> 28 << 3;

    tvaddr = tvaddr | PTE_SELF_REF;
    addr = (uint64_t *)tvaddr; 
    return addr;
} 

static uint64_t* get_pde_entry(uint64_t vaddr)
{
    uint64_t tvaddr;
    uint64_t *addr;

    tvaddr  = vaddr << 16 >> 37 << 3;

    tvaddr = tvaddr | PDE_SELF_REF;
    addr = (uint64_t *)tvaddr; 
    
    return addr;
} 

static uint64_t* get_pdpe_entry(uint64_t vaddr)
{
    uint64_t tvaddr;
    uint64_t *addr;

    tvaddr  = vaddr << 16 >> 46 << 3;

    tvaddr = tvaddr | PDPE_SELF_REF;
    addr = (uint64_t *)tvaddr; 

    return addr;

}

static uint64_t* get_pml4_entry(uint64_t vaddr)
{
    uint64_t tvaddr;
    uint64_t *addr;

    tvaddr  = vaddr << 16 >> 55 << 3;

    tvaddr = tvaddr | PML4_SELF_REF;
    addr = (uint64_t *)tvaddr; 
    
    return addr;
}

void map_process(uint64_t vaddr, uint64_t paddr)
{
    uint64_t *pml4_entry, *pdpe_entry, *pde_entry, *pte_entry;
    uint64_t entry; 

    pml4_entry = get_pml4_entry(vaddr);
    pdpe_entry = get_pdpe_entry(vaddr); 
    pde_entry = get_pde_entry(vaddr);   
    pte_entry = get_pte_entry(vaddr);

    entry = (uint64_t) *(pml4_entry);

    if (entry & PTE_P) {
        entry = (uint64_t) *(pdpe_entry);

        if (entry & PTE_P) { 
            entry  = (uint64_t) *(pde_entry);

            if (entry & PTE_P) { 
                entry  = (uint64_t) *(pte_entry);

                if (entry & PTE_P) { 
                    deallocate_page((void *)paddr);
                } else {
                    *pte_entry = paddr | PTE_P|PTE_W|PTE_U;
                }

            } else {
                *pde_entry = ((uint64_t)allocate_page()|PTE_P|PTE_W|PTE_U);
                *pte_entry = paddr | flags;
            }

       } else {
            *pdpe_entry = ((uint64_t)allocate_page()|PTE_P|PTE_W|PTE_U); 
            *pde_entry = ((uint64_t)allocate_page()|PTE_P|PTE_W|PTE_U); 
            *pte_entry = ((uint64_t)allocate_page()|PTE_P|PTE_W|PTE_U); 
       }

    } else {
        *pml4_entry = ((uint64_t)allocate_page()|PTE_P|PTE_W|PTE_U); 
        *pdpe_entry = ((uint64_t)allocate_page()|PTE_P|PTE_W|PTE_U); 
        *pde_entry = ((uint64_t)allocate_page()|PTE_P|PTE_W|PTE_U); 
        *pte_entry =  ((uint64_t)allocate_page()|PTE_P|PTE_W|PTE_U); 
    }
}


uint64_t set_user_Addrspace()
{
    uint64_t vaddr, paddr, *new_pml4;

    // Get a free virtual and physical page
    vaddr = kern_top_vaddr;
    physAddr = (uint64_t)allocate_page();

    map_process(vaddr, paddr);
    new_pml4 = (uint64_t *) vaddr;    
      
    // Reserve mapping for kernel page tables
    new_pml4[511] = pml[511];
    
    // Self referencing entry
    new_pml4[510] = (paddr|PTE_P|PTE_U|PTE_W);

    return paddr;

}
