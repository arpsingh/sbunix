/* © Copyright 2015, ggehlot@cs.stonybrook.edu
	 arpsingh@cs.stonybrook.edu,smehra@stonybrook.edu
	 Everyone is permitted to copy and distribute verbatim copies
	of this license document, but changing it is not allowed.
*/
#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <string.h>

int
print_string(const char *s, char *str)
{
	int i, len;
	len = strlen(s);

	for(i = 0; i < len; i++)
		*str++ = *s++;

	return i; 
}

int
print_int(int num, char *str)
{
	char s[5], temp;
	int index = 0, j = 0;

	bzero(s, 5);
	while (num != 0) {
        	s[index++] = '0' + (num%10);
        	num/=10;
	}

	for (j = 0; j < index/2; j++) {
        	temp = s[j];
        	s[j] = s[index-1-j];
        	s[index-1-j] = temp;
	}

	for(j = 0; s[j]; j++)
		*str++ = s[j]; 

	return j; 
}

int
print_hex(int addr, char *str) {
	char arr[33]; 
	char *ptr = &arr[sizeof(arr)-1]; 
	*ptr = '\0'; 

	while(addr!=0)
	{ 
		*--ptr="0123456789abcdef"[addr%16]; 
		addr/=16;
	}

	while(*ptr)
		*str++ = *ptr++;

	return 1;
}

int
print_char(int ch, char *str) {

	*str++ = ch;

	return 1;
}

static int
vprintf(
	const char *format,
	va_list args)
{
	int printed = 0;
	char print_buf[1024];
	char *str = print_buf;

	for (; *format != 0; ++format) {
		if (*format == '%') {
			++format;

			if (*format == '\0') break;


			switch(*format) {

			case 's':
				print_string(va_arg(args, char *), str);
				printed++;
				break;

			case 'd':
				print_int(va_arg(args, int), str);
				printed++;
				break;

			case 'c':
				print_char(va_arg(args, int), str);
				printed++;
				break;

			case 'x':
				print_hex(va_arg(args, int), str);
				printed++;
				break;
			}
		} else {
			*str++ = *format;	
		}
	}

	*str = '\0';
	write(1, print_buf, strlen(print_buf));
	return 0;
}

int printf(const char *format, ...) {

	va_list val;

	va_start(val, format);

	/*
	while(*format) {
		//write(1, format, 1);
		++printed;
		++format;
	}*/

	return vprintf(format, val);

}

void recalculate_offset(char *new_pos)
{

	v_ptr = new_pos;
}
