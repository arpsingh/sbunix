
/* © Copyright 2015, ggehlot@cs.stonybrook.edu
 * arpsingh@cs.stonybrook.edu,smehra@stonybrook.edu
 * Everyone is permitted to copy and distribute verbatim copies
 * of this license document, but changing it is not allowed.
 */


#include <sys/vmmu.h>
#include <sys/process.h>
#include <sys/gdt.h>
#include <sys/string.h>
#include <sys/sbunix.h>
#include <sys/elf.h>
#include <sys/tarfs.h>
#include <sys/pmap.h>
#include <sys/syscall.h>
#include <sys/elf.h>

task_struct *fg_task;
task_struct *sleep_task;
task_struct *current;

uint64_t stack_pos;
 
int getProcessID()
{
	int id = 0;
	for(;id < MAX_PROCESS; id++) {
		if(processID[id] == 0) {
			processID[id] = 1;
			return id;
		}
	}
	return -1;
}


void init_process_map()
{
	int id = 0;
	for(; id < MAX_PROCESS; id++)
		processID[id] = 0;
}


void init_func(){
	printf("init scheduled");

	while(1)
		schedule();
}


void create_init_process()
{
	/* init process task struct */
	task_struct *init = (task_struct *) kmalloc();

	init->task_state = TASK_NEW;
	init->pid = getProcessID();
	init->ppid = 0;
	init->next = init;
	init->mm = NULL;
	init->sleep = 0;
	init->wait_on_child_pid = -1;
	init->cr3 = (uint64_t)get_CR3();
	init->rip = (uint64_t)&init_func;
	strcpy(init->pname,"init proc");

	void *kstack = (void *)kmalloc();
	init->rsp = init->kernel_stack = ((uint64_t)kstack +0x1000 - 16);

	current = init;

}

task_struct* create_user_process(char *filename)
{
	/* read the elf header from the provided binary */ 
	struct posix_header_ustar *file = get_binary(filename);

	if(file == NULL) {
		printf("Binary not found!\n");
		return NULL;
	}

	int pid = getProcessID();

	/* new user process */
	task_struct *newProc = (task_struct *)kmalloc();
	newProc->pid = pid;
	newProc->task_state = TASK_NEW;
	newProc->wait_on_child_pid = -1;
        strcpy(newProc->cur_dir,"/rootfs/bin");
        char temp[30];
        strcpy(temp,newProc->cur_dir);
        dir *ret_dir = sys_opendir(temp);
        newProc->cur_node = ret_dir->node;
        strcpy(newProc->pname, filename);

	
	newProc->cr3 = (uint64_t)set_user_AddrSpace();
	struct PML4 *curr_CR3 = (struct PML4 *)get_CR3();

	/* set the current CR3 value to new process pml4 address */
	set_CR3((struct PML4 *)newProc->cr3);


	/* allocate space to mm structure */ 
	mm_struct *mm = (mm_struct *)kmalloc();
	newProc->mm = mm;

	/* kernel stack, set rsp to the base address of the stack */ 
	void *kStack = (void *)kmalloc();
	newProc->kernel_stack = ((uint64_t)kStack + 4096 - 16);

	/* load the elf */
	int error = load_exe(newProc, (void *)(file + 1));

	if(error)
		return NULL;

	newProc->rsp -= 8;

	/* set the CR3 value back to the prev process pml4 */ 
	set_CR3((struct PML4 *)curr_CR3);

	/* make the new process as the current process */
	current->next = newProc;
	newProc->next = current;
	current = newProc; 

	return newProc;
}

void switch_to_ring3(task_struct *task)
{
	task->task_state = TASK_RUNNING;

       volatile unsigned int a = 0x2B;
        //printf("Value is %d", a);

        __asm volatile("movq %0,%%rax;\n\t"
            "ltr (%%rax);"::"r"(&a));


	tss.rsp0 = task->kernel_stack;
	


	__asm__ __volatile__ (
	"sti;"
        "movq %0, %%cr3;"
        "mov $0x23, %%ax;"
        "mov %%ax, %%ds;"
        "mov %%ax, %%es;"
        "mov %%ax, %%fs;"
        "mov %%ax, %%gs;"

        "movq %1, %%rax;"
        "pushq $0x23;"
        "pushq %%rax;"
        "pushfq;"
        "popq %%rax;"
        "orq $0x200, %%rax;"
        "pushq %%rax;"
        "pushq $0x1B;"
        "pushq %2;"
	"movq $0x0, %%rdi;"
	"movq $0x0, %%rsi;" 
        "iretq;"
        ::"r"(task->cr3), "r"(task->rsp),"r"(task->rip)
    );

}

void* copy_task_struct(task_struct *parent)
{
	task_struct *child_task = (task_struct *)kmalloc();

	child_task->task_state = TASK_NEW;
	child_task->pid = getProcessID();
	child_task->ppid = parent->pid ;
	child_task->next = NULL;
	child_task->mm = NULL;
	child_task->sleep = 0;
        child_task->wait_on_child_pid = -1;
	parent->nchild = 1;

	/* set child page tables */
	child_task->cr3 = (uint64_t)allocate_page();
	setup_child_pagetable(child_task->cr3);

	/* set the child CR3 to allocate memory to its structures*/
	set_CR3((struct PML4 *)child_task->cr3);

	/* copy file descriptors */
	memcpy(&(child_task->fd[0]),&(parent->fd[0]),(sizeof(parent->fd[0])* MAX_FD));

	/* copy mm_struct */ 
	child_task->mm = kmalloc();
	memcpy(child_task->mm, parent->mm, sizeof(mm_struct));

	/* copy vmas */
	vma_struct *parent_vma = parent->mm->mmap;
	vma_struct *child_vma = NULL;

	while(parent_vma) {

		child_vma = (vma_struct *)kmalloc();

		memcpy(child_vma, parent_vma, sizeof(vma_struct));

		if(parent_vma->file!=NULL){
			child_vma->file = kmalloc();
			memcpy(child_vma->file,parent_vma->file,sizeof(struct file));
		}

		if(child_vma->next)
			child_vma = child_vma->next;
		parent_vma = parent_vma->next;
	}

	/* if no vma was there in parent , mark the
	 * child memory map as null and return
	 */
	if(!child_vma) {
		child_task->mm->mmap = NULL;
		goto ret;
	}		

	child_vma->next = NULL;


ret:
	return (void *)child_task;

}

int sys_fork()
{
	task_struct *temp, *parent = NULL;	

	/* copy parent's task_struct to Child task structure */
	task_struct *child = (task_struct *)copy_task_struct((task_struct*)current);

	/* queue the child task just after the parent(current) task */
	temp = current->next;
	current->next = child;
	child->next = temp;

	parent = current;

	void *kstack = (void *)kmalloc();
        child->kernel_stack = ((uint64_t)kstack + 4096 -16);
        memcpy((void *)(child->kernel_stack - 0x1000 +16), (void *)(parent->kernel_stack - 0x1000 +16), 4096);

	/* load back the parent CR3 */
	set_CR3((struct PML4 *)parent->cr3);

	/* Below assembly copy the current rsp
	 * to the variable to adjust the child's
	 * kernel stack position and the and load
	 * the next instruction pointer in the child's
	 * rip, so that when child  is scheduled to 
	 * execute, it starts from the next instruction
	 */ 
	__asm__ __volatile__ (
       		 "movq %%rsp, %1;" 
	         "movq $2f, %0;" 
       		 "2:\t"
	         :"=g"(child->rip),"=m"(stack_pos)
    	);
     
	if(current == parent) {
		/* if parent is executing, set the child kernel stack
		 * before leaving the ring 0
		 */
		stack_pos = (parent->kernel_stack - stack_pos);
       		return child->pid;
	}
	else {
		/* clear this pos used only by the child */
		stack_pos = 0;
       		outb(0x20, 0x20);
        	return 0;
  	}
}

void switch_to(task_struct *next, task_struct *prev) {

	tss.rsp0 = next->kernel_stack;

	__asm__ __volatile__ (
	"sti;"
        "movq %%rsp, (%1);"     // save RSP
        "movq $1f, %0;"         // save RIP
        "movq %2, %%rsp;"       // load next task's kernel stack in rsp
        "movq %4, %%cr3;"       // load next task's page table base addr into cr3
        "pushq %3;"             // restore RIP so as to continue execution in next task
        "retq;"                 // Switch to new task
        "1:\t"
        :"=g"(prev->rip)
        :"r"(&(prev->kernel_stack)), "r"(next->kernel_stack - stack_pos), "r"(next->rip), "r"(next->cr3)
    );
}



void schedule()
{
	task_struct *prev =  (task_struct*)current;

	current = current->next;
	if(current != prev)
		switch_to( (struct task_struct*)current, prev);

}


void free_memory_map(mm_struct *mm)
{
	if(mm->mmap == NULL)
		return;

	vma_struct *vma = mm->mmap;
	while(vma != NULL) {
		vma_struct *free_vma = vma;
		vma = vma->next;
		kfree((uint64_t)free_vma);
	}	
	
}


int sys_execve(char *filename, char **argv, char **envp)
{
	int i=0, argc=0;

	char copy_argv[5][24];
	
	for (i = 0; i < 5; ++i)
       		copy_argv[i][0] = '\0';

	if(argv != NULL){
		while(argv[argc] != NULL){
               		strcpy(copy_argv[argc],argv[argc]);
               		argc++;
        	}
	}
    	  
	/* create a new task structure */
	task_struct *task = (task_struct *)kmalloc();

	/* copy pid and ppid */
	task->pid = current->pid;
	task->ppid = current->ppid;

	/* copy file descriptors */ 
	memcpy(&(task->fd[0]),&(current->fd[0]),(sizeof(current->fd[0])* MAX_FD));

        /* read the elf header from the provided binary */
        struct posix_header_ustar *file = get_binary(filename);

        if(file == NULL) {
                printf("Binary not found!\n");
                return -1;
        }

	task->cr3 = (uint64_t)set_user_AddrSpace();
        struct PML4 *curr_CR3 = (struct PML4 *)get_CR3();

	set_CR3((struct PML4 *)task->cr3);

        /* allocate a new page to the kernel stack */
        void *kstack = (void *)kmalloc();
        task->kernel_stack = ((uint64_t)kstack + 4096 - 16);

        /* allocate space to mm structure */
        mm_struct *mm = (mm_struct *)kmalloc();
        task->mm = mm;

        /* load the elf */
        int error = load_exe(task, (void *)(file + 1));

        /* return if exe is not loaded properly */
        if(error)
                return -1;

	void *ptr = (void *)(USER_STACK_TOP + 0x1000 - 16 - sizeof(copy_argv));
        memcpy(ptr, (void *)copy_argv, sizeof(copy_argv));

        for(i=argc;i>0;i--){

		*(uint64_t*)(ptr - 8*i) = (uint64_t)ptr + (argc-i)*24;
        }

        ptr = ptr- 8*argc;
        task->rsp = (uint64_t)ptr;

	set_CR3((struct PML4 *)curr_CR3);

	/* free memory allocated to vmas */
        free_memory_map(current->mm);

        /* All the vmas are freed, now free the memory map */
        kfree((uint64_t)current->mm);
        current->mm = NULL;

        /* free the page  tables */
        delete_pagetable(current->cr3);

	task_struct *prev = current->next;
	while(prev->next != current)
		prev = prev->next;
	

	prev->next = task;
	task->next = current->next;

	task_struct *temp = current;
	current = task;
	kfree((uint64_t)temp);


        /* set the task state segment register */
        tss.rsp0 = task->kernel_stack;

	__asm__ volatile("movq %0, %%cr3"::"r"(task->cr3));


	 __asm__ __volatile__ (
	"sti;"
        "movq %3, %%rsp;"
        "mov $0x23, %%ax;"
        "mov %%ax, %%ds;"
        "mov %%ax, %%es;"
        "mov %%ax, %%fs;"
        "mov %%ax, %%gs;"

        "movq %1, %%rax;"
        "pushq $0x23;"
        "pushq %%rax;"
        "pushfq;"
        "popq %%rax;"
        "orq $0x200, %%rax;"
        "pushq %%rax;"
        "pushq $0x1B;"
        "pushq %2;"
        "movq %4, %%rsi;"
        "movq %5, %%rdi;"
        "iretq;"
        ::"r"(task->cr3), "r"(task->rsp), "r"(task->rip), "r"(task->kernel_stack),
        "m"(ptr), "m"(argc) 
    );
    return -1;
	

}

void nano_sleep(uint64_t stime)
{
	current->sleep = stime;
	task_struct *temp = current;

	/* remove it from the ready list */
	while(temp->next !=  current) {
		temp = temp->next;	
	}

	temp->next = current->next;

	/* add it to the sleeping list */
	current->next = sleep_task;
	sleep_task = current;
	sleep_task->task_state = TASK_WAITING;

	/* mark the next task as the current */
	current = temp->next;
	schedule();
} 
	 
void decrement_sleep_time()
{

	if(sleep_task == NULL)
		return;
 
	task_struct *task, *temp, *cur;
	temp = sleep_task;
	task = current;

	if(temp->sleep <= 0 && temp->wait_on_child_pid == -1) {

		cur = task->next;
		task->next = temp;
		sleep_task = temp->next;
		temp->next = cur;

		temp->task_state = TASK_RUNNING;
	}
	while(temp->next) {
		uint64_t s_time = temp->next->sleep;
		s_time--;
		temp->next->sleep = s_time;
	
		if(temp->next->sleep <= 0 && temp->wait_on_child_pid == -1) {
			cur = task->next;
			task->next = temp->next;
			sleep_task = temp->next->next;	
			temp->next->next = cur;

			temp->task_state = TASK_RUNNING; 
		} 
		
		temp = temp->next;
	}

}

/* static void exit_task(task_struct *task)
{
	free_memory_map(task->mm);
	kfree((uint64_t)task->mm);
	task->mm = NULL;
	
	delete_pagetable(task->cr3);

	kfree((uint64_t)task->kernel_stack);

	kfree((uint64_t)task);

} */

void exit(int exit_status)
{

	task_struct *task_remove = current;
	task_struct *ptr = current->next;
	task_struct *parent = sleep_task;

	int found = 0;	
	task_struct *temp = ptr;

	while(ptr != task_remove){
		temp = ptr;
		ptr = ptr->next;
	}
	if (ptr == task_remove)
		ptr = temp;

	if (sleep_task != NULL) {
	while(parent->next) {

                if(parent->next->wait_on_child_pid == current->pid)
                {
                        parent->next->wait_on_child_pid = -1;
                        parent->next = parent->next->next;
                       	ptr->next = parent->next;
			parent->nchild -= 1;
			parent->next->task_state = TASK_RUNNING;
			found = 1; 
                }
                parent = parent->next;
        }

	if(parent->wait_on_child_pid == current->pid) {
		parent->wait_on_child_pid = -1;
		ptr->next = parent;
		parent->next = current->next;
		parent->nchild -= 1;

		parent->task_state = TASK_RUNNING;
		
		found = 1;		
	}
	}

	if(!found)
		ptr->next = current->next;
	temp = current;
	current = ptr;
	

	//exit_task(temp);

	schedule();

} 
