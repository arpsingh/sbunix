/* © Copyright 2015, ggehlot@cs.stonybrook.edu
	 arpsingh@cs.stonybrook.edu,smehra@stonybrook.edu
	 Everyone is permitted to copy and distribute verbatim copies
	of this license document, but changing it is not allowed.
*/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define MAX_LEN		1024

static char *child_env[50], *path_lookup[30];
char ***cmd_args;
typedef void (*sighandler_t)(int);

char **environ;

char PS1[50];

/*
 * get the environment variables and store the
 * PATH variable string to search for binaries
 */

void
get_env(
	char **envp,
	char *path)
{
	int count = 0;
	char *path_string = NULL;
	char *temp;

	while(envp[count] != NULL) {
		child_env[count] = (char *)malloc(sizeof(char) * (strlen(envp[count]) + 1));
		strncpy(child_env[count], envp[count], strlen(envp[count]));

		
		if(!strncmp(child_env[count], "PATH", 4)) {
			strncpy(path, child_env[count], strlen(child_env[count]));
		}
		count++;
	}

	path_string = path;

	while(*++path_string != '=');

	temp = ++path_string;
	for(count = 0; *path_string;) {
                if(*path_string == ':') {
			
                        path_lookup[count] = (char *) malloc(sizeof(char) * strlen(temp+ 1));
                        strncpy(path_lookup[count], temp, (path_string-temp));
			strncat(path_lookup[count], "/", 1);
                        strncat(path_lookup[count], "\0", 1);
			temp = ++path_string;
			count++;
			continue;
                }
                path_string++;
        }
	path_lookup[count] = (char *) malloc(sizeof(char) * (strlen(temp) + 1));
	strncat(path_lookup[count], temp, strlen(temp));
	strncat(path_lookup[count], "/\0", 2);

	path_lookup[count+1] = NULL;

	
	
}

/*
 * Environment variables set routine
 */

void
set_env(
	char *env_name,
	char *env_value)
{
	int index = 0, found = 0;
	char *new_value = NULL;
	char **ep;

	for(index = 0, ep = environ; *ep; index++, ++ep) {
		
		/*
		 * search if the given variable is present in the environment
		 * variable's list
		 */

		if(!strncmp(child_env[index], env_name, strlen(env_name))) {
			/*
			 * PATH variable -  append the string
			 */

			if(!strncmp(env_name, "PATH", 4)) { 
				new_value = (char *)malloc(sizeof(char) * (strlen(child_env[index])+ strlen(env_value)));
				strncpy(new_value, child_env[index], strlen(child_env[index]));
				strncat(new_value, ":", 1);
				strncat(new_value, env_value, strlen(env_value));
			
				child_env[index] = new_value;
				*ep = (char *)new_value;
				found = 1;
				
				break;
			}
		}
	}

	/*
	 * if not found add it to the list
	 */

	if(!found) {
		printf("%s\n", env_value);
		bzero(PS1, 50);
           	strncpy(PS1, env_value, strlen(env_value));
	}
}

void
exec_commands(
        char** cmds[],
        size_t pos,
        int in_fd)
{
	/*
	 * check if this is the last command to be executed
	 */

        if (cmd_args[pos + 1] == NULL) {
                dup2(in_fd, 0);
                if(execve(cmds[pos][0], cmds[pos], child_env) == -1) {
                        printf("Command not found\n");
                        exit(1);
                }
        }
        else {
                int fd[2];
                if (pipe(fd) == -1)
                        exit(1);

                int pid = fork();
                if(pid == 0) {
			//child
                        close(fd[0]);
                        dup2(in_fd, 0);
                        dup2(fd[1], 1);
                        execve(cmds[pos][0], cmds[pos], child_env);
                } else if (pid > 0){
			//parent
                        close(fd[1]);
                        close(in_fd);
                        exec_commands(cmds, pos + 1, fd[0]);
                } else {
			//error
                        exit(1);
                }
        }
}

int
parse_cmd(
	char *user_input)
{
	char *number_commands[10], *cmds[20];
	char *token = NULL, temp[100];
	int count = 0, i = 0, k =0;
	int counter = 0, fd;

	/*
	 * Set/Unset Environment Variables
	 */

	if(!strncmp(user_input, "set", 3)) {
                token = strtok(user_input, " ");

                while(token != NULL) {
                        cmds[counter] = (char *)malloc(sizeof(char)*(strlen(token)+1));
                        strcpy(cmds[counter], token);
                        token = strtok(NULL, "=\n");
                        counter++;
                }

                set_env(cmds[1], cmds[2]);

                while(counter--)
                        free(cmds[counter]);

                return 1;
        }


	token = strtok(user_input, "|\n");

	if(token == NULL) {
		return 0;
	}

	/*
	 * Exit the Shell
	 */

	if(!strcmp(token, "exit")) {
		exit(0);
	}

	/*
	 * Change Directory
	 */
 
	if(!strncmp(token, "cd", 2)) {
		while(*token != ' ') {
			if(*token == '\t') 
				return 0;
			token++;
		}
		token++;

		if (chdir(token)) {
			printf("error\n");
		}
		
		return 1;
	}
	
	cmd_args = (char ***)malloc(sizeof(char **) * 10);

	/*
	 * array of commands
	 */

	while(token != NULL) {
		if(*token == ' ')
			token++;
		number_commands[count] = (char *)malloc(sizeof(char)*(strlen(token)+1));
		strcpy(number_commands[count], token);
		strncat(number_commands[count], "\0", 1);
		cmd_args[count] = (char **)malloc(sizeof(char *) * (strlen(number_commands[count])+10));
		token = strtok(NULL, "|\n");
		count++;
	}
	number_commands[count] = "\0";

	/*
	 * Separate the command from its arguments
	 */

	while (k < count) {
		token = strtok(number_commands[k], " \n");
		counter = 0;
		while(token != NULL) {
			cmds[i] = (char *)malloc(sizeof(char)*(strlen(token)+1));
			strcpy(cmds[i], token);

			cmd_args[k][counter] = (char*)malloc(sizeof(char) * strlen(cmds[i])+1);
			strncpy(cmd_args[k][counter], cmds[i], strlen(cmds[i]));
			strncat(cmd_args[k][counter], "\0", 1);
			
			free(cmds[i]);

			token = strtok(NULL, " \n");
			i++;
			counter++;
		}
		k++;
	}
	cmds[i] = NULL;

	/*
	 * If absolute path si not specified, go through the PATH
	 * variable, look for the binary
	 * If successful prepend the path to the command
	 */

	for(counter = 0; counter < k; counter++) {
		free(number_commands[counter]);

		 if(index(cmd_args[counter][0], '/') == NULL) {
                        for(count=0; path_lookup[count] != NULL; count++) {
				bzero(temp, 100);
                                strncpy(temp, path_lookup[count], strlen(path_lookup[count]));
                                strncat(temp, cmd_args[counter][0], strlen(cmd_args[counter][0]));
                                if((fd = open(temp, O_RDONLY)) > 0) {
                                        strncpy(cmd_args[counter][0], temp, strlen(temp));
                                        close(fd);
                                        break;
                                }
                        }
                }
	}

	return i;
}

/*
 * Sbush entry point
 */

int
main(
	int argc,
	char **argv,
	char **envp )
{

	int pos = 0;
	char buffer[MAX_LEN];
	char *path = (char *)malloc(sizeof(char) * 1024);

	int i = 0, status;
	pid_t child_pid = -1;

	strncpy(PS1, "[Sbush $$] >> ", strlen("[Sbush $$] >> "));

	/*get environment variables from the main's
	 * argument vector
	 */

	get_env(envp, path);
	environ = child_env;

	cmd_args = (char ***)malloc(sizeof(char **) * 10);

	while(1) {
		printf("%s", PS1);
		scanf("%s", buffer);

		/*
		 * parse the command line arguments
		 */

		pos = parse_cmd(buffer);
		bzero(buffer, 1024);
		
		/*
		 * No command entered, next line
		 */
	        if(pos == 0) {
			continue;
		}

		/*
		 * Create a child process and run the command
		 * requested by the user
		 */

		child_pid = fork();
		if(child_pid == 0) {
			//execute the given commands
			exec_commands(cmd_args, 0, 0);
		} else {
			//wait for the child
			waitpid(child_pid, &status, 0);
		}
	}

	/*
	 * free the allocated memory
	 */

	free(path);

	for(i=0;child_env[i]!=NULL;i++)
		free(child_env[i]);

	for(i=0;path_lookup[i]!=NULL;i++)
		free(path_lookup[i]);

	return 0;
}
