#include <stdlib.h>
#include <sys/defs.h>
#include <string.h>

int main(int argc,char* argv[])
{
        uint64_t pid;

        if (argc != 2)
                return 0;

        pid = str_to_int(argv[1]);
        if (pid > 0)
                kill(pid);

        return 0;

}

