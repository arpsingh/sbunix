/* © Copyright 2015, ggehlot@cs.stonybrook.edu
	 arpsingh@cs.stonybrook.edu,smehra@stonybrook.edu
	 Everyone is permitted to copy and distribute verbatim copies
	of this license document, but changing it is not allowed.
*/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define MAX_LEN		1024

char path[50];
char filename[50];
char *exec_args[5];


char PS1[50];


void exec_command() {

	int pid = fork();
	
	if (pid != 0) {
		waitpid(pid,NULL,0);
	} else {
		execve(filename,exec_args,NULL);
		exit(1);

	}

}

int
parse_cmd(
	char *user_input)
{
	//char *number_commands[10], *cmds[20];
	char *token = NULL;
	//char  temp[100];
//	int count = 0;
	int i=0,k=0,j=0;
//	int counter = 0 ;
        char cmd[5][64];
       if (!strcmp(user_input,"echo $PATH"))
                printf(" %s \n",path);
        if (!strcmp(user_input,"echo $PS1"))
                printf(" %s \n" ,PS1);
	int len = strlen(user_input);
		
       for (k=0;k <= len;k++) {
	if (user_input[k] == ' ') {
		if (k != 0){ 
			cmd[i][j] = '\0';
			j=0;
			i++;
		}
		char *newst = user_input;
		newst = newst+k+1;
		while ((*newst == ' ') && (k < len)){
			k++;	
			newst++;
		}
	} else {
		cmd[i][j] = user_input[k];
		j++;
	  } 
	}
	if (cmd[0] == '\0')
		return 0; 

	if ((strcmp(cmd[0], "/rootfs/bin/ls") == 0) || (strcmp(cmd[0],"ls") == 0)) {
		strcpy(filename,"bin/ls");
		exec_command();	

	} else if ((strcmp(cmd[0], "/rootfs/bin/ps") == 0) || (strcmp(cmd[0],"ps") == 0)){
		strcpy(filename,"bin/ps");
		exec_args[0] = (char *)NULL;
		exec_command();
	} else if ((strcmp(cmd[0], "/rootfs/bin/sleep") == 0) || (strcmp(cmd[0],"sleep") == 0)){
		strcpy(filename,"bin/sleep");
		exec_args[0] = (char *)cmd[1];
		exec_command();

	} else if (strcmp(cmd[0], "cd") == 0) {
			char *st = cmd[1];	
			if (*st == ' ' || *st == '\0')
				strcpy(cmd[1],"/rootfs/..");
		if (chdir(cmd[1]) > 0) {
                         bzero(PS1,50);
                         char *str = PS1;
                         int ps1_size=strlen("user:~");
                         strncpy(str, "user:~", ps1_size);
                         str +=ps1_size;
                         getcwd(str,40);
                         str++;
                         ps1_size = strlen(PS1);
                         PS1[ps1_size] = '$';

                } else {
                        printf("No such file or directory \n");
                }




	} else if (strcmp(cmd[0],"help") == 0) {
		int fd1;
        fd1 = open("rootfs/etc/help.txt",O_RDWR);
        char buf[1024];
        read(fd1,buf,1024);
        printf("-----------HELP-----------\n");
	token = strtok(buf,"\n");
	while (token != NULL)
	{  char *str = token;
	
		while(*str == ' ' && *str) str++;
		printf("%s \n",str);
		token = strtok(NULL,"\n");
	}
	} else if (strcmp(cmd[0],"clear") == 0) {

		clear();
	} else if (strcmp(cmd[0],"export") == 0) {
		if (strncmp(cmd[1],"PATH=",5) == 0){
			char *str=cmd[1];
			str = str+5;
			strcpy(path,str);
		} else if (strncmp(cmd[1],"PS1=",4) == 0){
                        char *str=cmd[1];
                        str = str+4;
                        strcpy(PS1,str);
		}
	} else if (strcmp(cmd[0],"pwd") == 0) {
		char buf[50];
		getcwd(buf,50);
		printf(" %s \n",buf);
	} 
	


	token = strtok(user_input, "|\n");

	if(token == NULL) {
		return 0;
	}

	/*
	 * Exit the Shell
	 */

	if(!strcmp(token, "exit")) {
		exit(0);
	}


	return i;
}

/*
 * Sbush entry point
 */

int
main(
	int argc,
	char **argv,
	char **envp )
{

	int pos = 0;
	char buffer[MAX_LEN];
//	char *path = (char *)malloc(sizeof(char) * 1024);
	strcpy(path,"/rootfs/bin/");

//	int i = 0, status;
//	pid_t child_pid = -1;

	//strncpy(PS1, "[Sbush $$] >> ", strlen("[Sbush $$] >> "));
	char *str = PS1;
	int ps1_size=strlen("user:~");
	strncpy(str, "user:~", ps1_size);
	str +=ps1_size;
	getcwd(str,40);
	str++;
	ps1_size = strlen(PS1);
	PS1[ps1_size] = '$'; 

	/*get environment variables from the main's
	 * argument vector
	 */

	//get_env(envp, path);
	//environ = child_env;


	while(1) {
		printf("%s", PS1);
		scanf("%s", buffer);

		/*
		 * parse the command line arguments
		 */

		pos = parse_cmd(buffer);
		bzero(buffer, 1024);
		
		/*
		 * No command entered, next line
		 */
	        if(pos == 0) {
			continue;
		}

		}
	return 0;
}
